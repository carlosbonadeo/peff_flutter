class Sponsor {
  String url;
  String nombre;
  String img;

  Sponsor(this.nombre, this.img, [this.url]);

  /// Path de la imagen en la página web
  String get imgPath {
    return "http://www.patagoniaecofilmfest.com/peff/img/$img";
  }
}
