import 'package:intl/intl.dart';

class Day {
  /// ID del día del festival
  int id;

  /// Fecha en formato DateTime
  DateTime date;

  /// Imagen de fondo para el Carrousel
  String img;

  /// Breve descripción de las actividades del día
  String desc;

  Day(this.id, this.date, this.img, this.desc);

  /// Fecha en formato human-friendly
  @override
  String toString() {
    return DateFormat('EEEE dd/MM', 'es_AR').format(date);
  }

  /// Devuelve si hoy es el día del festival
  bool get isToday {
    DateTime today = DateTime.now();
    return date.difference(today).inDays == 0 && date.day == today.day;
  }
}
