class Staff {
  String name;
  String role;
  String img;

  Staff(this.name, this.role, this.img);

  /// Path de la imagen en la página web
  String get imgPath {
    return "http://www.patagoniaecofilmfest.com/peff/img/$img";
  }
}