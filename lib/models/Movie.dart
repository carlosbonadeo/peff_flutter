// To parse this JSON data, do
//
//     final movie = movieFromJson(jsonString);

import 'dart:convert';
import 'package:intl/intl.dart';

List<Movie> movieFromJson(String str) {
  final jsonData = json.decode(str);
  return new List<Movie>.from(jsonData.map((x) => Movie.fromJson(x)));
}

String movieToJson(List<Movie> data) {
  final dyn = new List<dynamic>.from(data.map((x) => x.toJson()));
  return json.encode(dyn);
}

class Movie {
  int day;
    String synopsis;
    String trailer;
    String location;
    String startDate;
    String thumbnail;
    String cover;
    String extra;
    String country;
    String director;
    String yearRelease;
    String title;
    int id;

    Movie({
        this.day,
        this.synopsis,
        this.trailer,
        this.location,
        this.startDate,
        this.thumbnail,
        this.cover,
        this.extra,
        this.country,
        this.director,
        this.yearRelease,
        this.title,
        this.id,
    });

    factory Movie.fromJson(Map<String, dynamic> json) => Movie(
        day: json["day"],
        synopsis: json["synopsis"],
        trailer: json["trailer"],
        location: json["location"],
        startDate: json["startDate"],
        thumbnail: json["thumbnail"],
        cover: json["cover"],
        extra: json["extra"],
        country: json["country"],
        director: json["director"],
        yearRelease: json["yearRelease"],
        title: json["title"],
        id: json["id"],
    );

    Map<String, dynamic> toJson() => {
        "day": day,
        "synopsis": synopsis,
        "trailer": trailer,
        "location": location,
        "startDate": startDate,
        "thumbnail": thumbnail,
        "cover": cover,
        "extra": extra,
        "country": country,
        "director": director,
        "yearRelease": yearRelease,
        "title": title,
        "id": id,
    };

  DateTime get toStartDateTime {
    return DateTime.parse(startDate);
  }

  String get toStringDate {
    return DateFormat('EEEE dd/MM, HH:mm', 'es_AR')
        .format(this.toStartDateTime);
  }

  // bool get isEnded {
  //   DateTime movieDate = DateTime.parse(this.startDate);
  //   return !DateTime.now().compareTo(movieDate).isNegative;
  // }
}
