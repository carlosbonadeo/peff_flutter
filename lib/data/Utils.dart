import 'dart:ui';

import 'package:peff_flutter/models/Day.dart';
import 'package:peff_flutter/models/Sponsor.dart';

class Utils {
  static Color primaryColor = Color(0xFF8DC9A5);
  static Color primaryDarkColor = Color(0xFF77AA8B);
  static Color orange = Color(0xFFEA5B23);

  static bool isToday(DateTime dateA, DateTime dateB) {
    if (dateA.difference(dateB).inDays == 0 && dateA.day == dateB.day)
      return true;
    return false;
  }

  static List<Day> dayList = [
    Day(1, DateTime(2019, 10, 02), "ganadores_2018/mencion_corto2",
        "Ceremonia de Apertura y proyección del film de apertura."),
    Day(2, DateTime(2019, 10, 03), "ganadores_2018/mencion_corto",
        "Proyección Especial para Escuelas. Retrospectiva Rob Stewart. Largometrajes."),
    Day(3, DateTime(2019, 10, 04), "alimentacion_2018",
        "Proyección Especial para Escuelas. Retrospectiva Rob Stewart. Largometrajes."),
    Day(4, DateTime(2019, 10, 05), "solar_2018",
        "Proyección Especial: MINI PEFF. Cine ambiental para niñ@s. Patagonia en foco. Ceremonia de cierre y proyección del film de cierre.")
  ];

  static List<Sponsor> organizan = [
    Sponsor("Unirse", "org/unirse.png", "https://www.facebook.com/unirseweb/"),
    Sponsor("Compromiso sustentable", "org/4rlogo.png",
        "https://www.facebook.com/4erre.ong/"),
    Sponsor("Pléyades cinema", "org/Pleyades.jpg"),
  ];

  static List<Sponsor> sponsors = [
    Sponsor("INCAA", "sponsor/incaa.png", "http://incaa.gov.ar/"),
    Sponsor("Camuzzi Gas", "sponsor/camuzzi.jpg", "http://www.camuzzigas.com/"),
    Sponsor("Genneia", "sponsor/genneia.png", "http://www.genneia.com.ar/"),
    Sponsor("Coca-cola", "sponsor/coca.png",
        "http://www.coca-cola.com.ar/es/home/"),
    Sponsor("Henkel", "sponsor/henkel.png", "http://www.henkel.com.ar/"),
    Sponsor("Nautico", "sponsor/bistro.png",
        "https://www.facebook.com/NauticoBistroDeMar/"),
    Sponsor("Banco del Chubut", "sponsor/banco_chubut.png",
        "https://www.bancochubut.com.ar/"),
    Sponsor("Puerto Madryn", "sponsor/entemixto.png",
        "http://www.madryn.gov.ar/entemixtodeturismo/"),
    Sponsor("Electromecánica sur", "sponsor/electromecanica.png",
        "https://www.electromecanicasur.com/"),
    Sponsor("Portal del Madryn", "sponsor/portal.png",
        "http://www.portaldemadryn.com"),
    Sponsor("Aht Chubut", "sponsor/aht.png", "https://ahtra.com.ar/"),
    Sponsor(
        "Carrefour", "sponsor/carrefour.png", "http://www.carrefour.com.ar/"),
    Sponsor("Prudential", "sponsor/prudential.png",
        "https://www.prudentialseguros.com.ar/"),
    Sponsor("Andreani", "sponsor/andreani.png", "http://www.andreani.com/"),
    Sponsor("Bottazzi", "colaboran/botazzi.jpg", "http://www.bottazzi.com.ar/"),
  ];

  static List<Sponsor> colaboran = [
    Sponsor(
        "Tolosa", "colaboran/tolosa.png", "https://www.hoteltolosa.com.ar/"),
    Sponsor("Hotel Territorio", "sponsor/territorio.png",
        "http://hotelterritorio.com.ar/"),
    Sponsor("Municipalidad de Puerto Madryn", "colaboran/muni.png",
        "https://www.madryn.gob.ar/"),
    Sponsor("Lobo Larcen", "colaboran/lobo.png", "http://www.lobolarsen.com/"),
    Sponsor("Amser", "apoyan/amser.png", "http://www.amser.com.ar/"),
    Sponsor("Madryn en bici", "apoyan/bici.png",
        "https://www.facebook.com/madrynenbici/?fref=ts"),
    Sponsor("V Studio", "colaboran/VStudioLogo.png",
        "https://www.facebook.com/VStudioDG/"),
    Sponsor("Patagonia Multimedia", "colaboran/patagonia-multimedia.png",
        "http://patagoniamultimedia.com.ar/"),
    Sponsor("KM Sur", "colaboran/km-sur.png", "http://kmsur.com/"),
    Sponsor("Guanaco cervecería artesanal", "colaboran/guanaco.png",
        "https://www.facebook.com/GuanacoCerveceriaArtesanal/"),
    Sponsor("CIT Puerto Madryn", "colaboran/cit.png",
        "https://www.facebook.com/CITPuertoMadryn2019/"),
    Sponsor("Quero vasos", "colaboran/qero.png", "http://ecovasos.com/"),
    Sponsor("Madryn ilustrado", "colaboran/madryn-ilustrado.png",
        "https://madrynilustrado.com/"),
    Sponsor(
        "Margarita", "colaboran/margarita.png", "http://www.margaritapub.com/"),
    Sponsor("Soho", "colaboran/soho.png",
        "https://www.facebook.com/pages/category/Business-Service/Soho-English-Institute-578817379283547/"),
    Sponsor("Oceano patagonia", "colaboran/oceano-color.png",
        "http://oceanopatagonia.com/es/"),
    Sponsor("Unitecnica", "colaboran/unitecnica.png",
        "https://www.facebook.com/Unitecnica-2092064264166405/"),
    Sponsor("Safari", "colaboran/safari.png", ""),
    Sponsor("ProyectoSub", "colaboran/proyecto-sub.png",
        "http://www.proyectosub.com.ar/sudamerica/"),
    Sponsor("OK Buceo", "colaboran/ok-buceo.png",
        "https://www.facebook.com/okbuceo/?__tn__=%2Cd%2CP-R&amp;eid=ARB4HQ-m4PgihHDp0UgjeyhNnRIUS0BvGUKaRg_v4VSf8SlozcctO8rKOXSBQoXVjWpGZT6T3X7W7EX4"),
    Sponsor("Entre el sol y la luna", "colaboran/sol-luna.png",
        "https://www.facebook.com/EntreSolyLaLuna/?rf=132785543858296"),
  ];

  static List<Sponsor> mediaPartners = [
    Sponsor("Gerencia ambiental", "mediapartners/gerenciaambiental.jpg",
        "http://gerencia-ambiental.com/"),
    Sponsor("Agenda social", "mediapartners/agendasocial.jpg",
        "http://www.agendasocialweb.com.ar/"),
    Sponsor("Alerta verde", "mediapartners/alerta_verde.jpg",
        "https://twitter.com/alerta_verde?lang=es"),
    Sponsor("Noticias ambientales", "mediapartners/noticias_ambientales.png",
        "http://noticiasambientales.com.ar/"),
    Sponsor("Futuro sustentable", "mediapartners/futuro.jpg",
        "http://www.futurosustentable.com.ar"),
    Sponsor("Innovar", "mediapartners/innovar.jpg",
        "http://www.innovarsustentabilidad.com/"),
    Sponsor("Canal 12", "mediapartners/canal12.png",
        "http://madryntv.com/canal12.php"),
    Sponsor("Visión sustentable", "mediapartners/vision.jpg",
        "http://www.visionsustentable.com/"),
    Sponsor("Lu17 AM", "mediapartners/Lu17.png", "https://lu17.com/"),
    Sponsor("Paraíso FM", "mediapartners/paraiso.png",
        "http://lu17.com/paraisofm/"),
    Sponsor("Sustentar TV", "mediapartners/sustentar.png", "https://lu17.com/"),
    Sponsor("Tercer sector", "mediapartners/tercer_sector.png",
        "https://tercersector.org.ar/"),
    Sponsor("Tres mandamientos", "mediapartners/3mandamientos.png",
        "http://www.tresmandamientos.com.ar/"),
    Sponsor("Patagonia ambiental", "mediapartners/patagonia_ambiental.png",
        "http://patagoniambiental.com.ar/info/"),
    Sponsor("Puerto Madryn Map", "mediapartners/ppm.png",
        "https://puertomadrynmap.com/"),
  ];

  static List<Sponsor> nosApoyan = [
    Sponsor("Aves argentinas", "apoyan/avesargentinas.png",
        "http://www.avesargentinas.org.ar"),
    Sponsor("Afona", "apoyan/afona.png", "http://afona.com.ar/"),
    Sponsor("Cinear", "apoyan/cinear.png", "http://www.cine.ar/"),
    Sponsor(
        "Ecocentro", "apoyan/ecocentro.jpg", "http://www.ecocentro.org.ar/"),
    Sponsor("FARN", "apoyan/farn.jpg", "http://farn.org.ar/"),
    Sponsor("Mar patagónico", "apoyan/mar_patagonico.png",
        "https://marpatagonico.org/"),
    Sponsor("Naturaleza para el futuro", "apoyan/funafu.jpg",
        "http://www.naturalezaparaelfuturo.org/"),
    Sponsor("GPS", "apoyan/Isologotipo.jpg",
        "http://www.globalpenguinsociety.org/"),
    Sponsor("Greenpeace", "apoyan/grande_logo.png",
        "http://www.greenpeace.org/argentina/es/"),
    Sponsor("OFS", "apoyan/ofs.png", "http://www.oceanfutures.org/"),
    Sponsor("SAE", "apoyan/sae.png", "http://www.saeditores.org/"),
    Sponsor("Avina", "apoyan/avina.png", "http://www.avina.net/avina/"),
    Sponsor("Cultura", "apoyan/cultura.png", "http://cultura.madryn.gov.ar/"),
    Sponsor(
        "Sea Shepherd", "apoyan/sea_shepherd.png", "https://seashepherd.org/"),
    Sponsor("Protejamos", "apoyan/protejamos.png",
        "http://protejamospatagonia.org/"),
    Sponsor("DAC", "apoyan/dac.png", "http://www.dac.org.ar/"),
    Sponsor("Sociedad italiana", "apoyan/sociedad.jpg",
        "http://www.cinemadryn.com/"),
    Sponsor("Cinema planeta", "apoyan/cinema_planeta.png",
        "https://cinemaplaneta.org/"),
    Sponsor("Big human wave", "apoyan/bighuman.png",
        "https://www.yoamomiplaya.org/"),
  ];
}
