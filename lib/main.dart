import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'dart:async';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:intl/date_symbol_data_local.dart';
import 'package:path_provider/path_provider.dart';
import 'package:peff_flutter/data/Utils.dart';
import 'package:peff_flutter/pages/CarrouselPage.dart';

import './pages/HomePage.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized(); 

  SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
      statusBarColor: Utils.primaryDarkColor,
      systemNavigationBarColor: Color(0xFF8DC8A5)));

  FlutterLocalNotificationsPlugin notification =
      new FlutterLocalNotificationsPlugin();

  var android = new AndroidInitializationSettings('@drawable/ic_notification');
  var ios = new IOSInitializationSettings();
  var settings = new InitializationSettings(android, ios);

  notification.initialize(settings,
      onSelectNotification: _onSelectedNotification);

  // Inicializa dos notificaciones para avisar que el festival va a comenzar
  _createNotifications(notification);
  // Si no existe, crea el archivo misPelis.json
  _initmyMoviesFileFile();

  SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp])
      .then((value) {
    initializeDateFormatting("es_AR", null)
        .then((value) => runApp(new MyApp()));
  });
}

Future<void> _initmyMoviesFileFile() async {
  Directory dir = await getApplicationDocumentsDirectory();
  File myMoviesFile = File("${dir.path}/myMovies.json");

  if (!myMoviesFile.existsSync()) {
    myMoviesFile.createSync();
    myMoviesFile.writeAsStringSync("[]");
  }
}

void _createNotifications(notification) {
  DateTime firstDay = Utils.dayList[0].date;
  DateTime oneWeek = firstDay.subtract(Duration(days: 7));
  DateTime oneDay = firstDay.subtract(Duration(days: 1));

  var android = new AndroidNotificationDetails('1', '2', '3',
      importance: Importance.Max,
      priority: Priority.High,
      color: Utils.primaryColor);
  var ios = new IOSNotificationDetails();
  var settings = new NotificationDetails(android, ios);

  notification.schedule(
    0,
    'Patagonia Eco Film Fest',
    'Falta poco para comenzar el festival, ¡no te lo pierdas!',
    oneWeek,
    settings,
  );

  notification.schedule(
    1,
    'Patagonia Eco Film Fest',
    'Falta un día para comenzar el festival, ¡nos vemos mañana!',
    oneDay,
    settings,
  );
}

Future _onSelectedNotification(String payload) async {
  print(payload);
}

Map<int, Color> color = {
  50: Color.fromRGBO(141, 201, 165, .1),
  100: Color.fromRGBO(141, 201, 165, .2),
  200: Color.fromRGBO(141, 201, 165, .3),
  300: Color.fromRGBO(141, 201, 165, .4),
  400: Color.fromRGBO(141, 201, 165, .5),
  500: Color.fromRGBO(141, 201, 165, .6),
  600: Color.fromRGBO(141, 201, 165, .7),
  700: Color.fromRGBO(141, 201, 165, .8),
  800: Color.fromRGBO(141, 201, 165, .9),
  900: Color.fromRGBO(141, 201, 165, 1),
};

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      title: 'PEFF',
      theme: new ThemeData(
        primarySwatch: MaterialColor(0xFF8DC8A5, color),
      ),
      home: new HomePage(
        current: 1,
        page: CarrouselPage(),
      ),
    );
  }
}
