import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:line_awesome_icons/line_awesome_icons.dart';
import 'package:peff_flutter/components/ExpansionTileCollapse.dart';
import 'package:peff_flutter/components/LoadingLayout.dart';
import 'package:peff_flutter/data/Utils.dart';
import 'package:peff_flutter/models/Day.dart';
import 'dart:io';
import 'package:path_provider/path_provider.dart';
import 'package:http/http.dart' as http;
import 'package:peff_flutter/models/Movie.dart';
import './MovieDetailPage.dart';

class MoviesPage extends StatefulWidget {
  final Day day;

  MoviesPage({this.day});

  @override
  _MoviesPageState createState() => _MoviesPageState();
}

class _MoviesPageState extends State<MoviesPage> {
  String url = 'https://api.jsonbin.io/b/5c3f421805d34b26f20b08a3/7';
  bool loading;
  File jsonFile;
  Directory dir;
  String fileName = "peliculas.json";
  List<Movie> cartelera = List<Movie>();
  Day day;
  List<Day> days;
  final GlobalKey<ExpansionTileCollapseState> expansionTileCollapse =
      new GlobalKey();
  double headerHeight = 30.0;

  @override
  void initState() {
    super.initState();

    day = widget.day ?? _getIdDay();
    days = Utils.dayList;

    getApplicationDocumentsDirectory().then((Directory directory) {
      dir = directory;
      jsonFile = File("${dir.path}/$fileName");

      if (jsonFile.existsSync()) {
        var carteleraList = jsonFile.readAsStringSync();
        _sortMovies(carteleraList);
      } else {
        setState(() {
          loading = true;
        });
        _createFile(jsonFile);
      }
    });
  }

  Day _getIdDay() {
    return Utils.dayList
        .firstWhere((day) => day.isToday, orElse: () => Utils.dayList[0]);
  }

  void _createFile(File file) {
    http.get(Uri.encodeFull(url), headers: {
      'Accept': 'application/json',
      'secret-key':
          '\$2a\$10\$6Z8/bonqo/yLHH460dBo7ultj8h7DWM/DtoM5naDavmUpIJslsLqW'
    }).then((response) {
      file.createSync();
      file.writeAsStringSync(response.body);

      _sortMovies(response.body);
    });
  }

  void _sortMovies(String carteleraStr) {
    if (mounted) {
      cartelera = movieFromJson(carteleraStr);
      cartelera.sort((a, b) => a.day.compareTo(b.day));
      setState(() {
        loading = false;
      });
    }
  }

  List<Movie> get _moviesByDay {
    List<Movie> moviesByDay =
        cartelera.where((movie) => day.id == movie.day).toList();
    moviesByDay.sort((a, b) => a.startDate.compareTo(b.startDate));
    return moviesByDay;
  }

  // Map<DateTime, List<Movie>> get _moviesGroupByHour {
  //   Map time;
  //   DateTime dateControl;
  //   _moviesByDay.forEach((movie) {
  //     DateTime movieDate = DateTime.parse(movie.startDate);
  //     if (dateControl == null)
  //       dateControl = movieDate;
  //     if (dateControl.compareTo(movieDate) == 0)
  //       time.addEntries(newEntries);
  //   });
  // }

  List<DateTime> get _dateTimeList {
    List<DateTime> dateTimeList = List();

    if (_moviesByDay.length != 0) {
      DateTime aux = _moviesByDay[0].toStartDateTime;
      dateTimeList.add(aux);

      _moviesByDay.forEach((movie) {
        if (movie.toStartDateTime.compareTo(aux) != 0) {
          dateTimeList.add(movie.toStartDateTime);
          aux = movie.toStartDateTime;
        }
      });
    }

    return dateTimeList;
  }

  List<Movie> moviesByHour(DateTime date) {
    return _moviesByDay
        .where((movie) => movie.toStartDateTime.compareTo(date) == 0)
        .toList();
  }

  Widget _gridByHour(DateTime date) {
    List<Movie> movies = moviesByHour(date);

    return Expanded(
      child: GridView.builder(
          shrinkWrap: true,
          primary: false,
          padding: EdgeInsets.all(6.0),
          gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
              childAspectRatio: 1.1, crossAxisCount: 2),
          itemCount: movies.length,
          itemBuilder: (context, i) => MovieTile(movie: movies[i])),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Material(
        color: Utils.primaryColor,
        child: Container(
          decoration: BoxDecoration(
              gradient: LinearGradient(
                  begin: Alignment.center,
                  end: Alignment.bottomCenter,
                  colors: [
                    Utils.primaryColor,
                    Colors.black12,
                  ]),
              color: Utils.primaryColor,
              image: DecorationImage(
                  fit: BoxFit.cover,
                  colorFilter: ColorFilter.mode(
                      Colors.black.withOpacity(.06), BlendMode.dstATop),
                  image: AssetImage('assets/flyer2019.png')
                  )),
          child: Column(
            mainAxisSize: MainAxisSize.max,
            children: <Widget>[
              SafeArea(
                child: Hero(
                  tag: day.id,
                  child: Card(
                    margin: EdgeInsets.all(12.0),
                    child: ExpansionTileCollapse(
                      key: expansionTileCollapse,
                      title: Container(
                        height: headerHeight,
                        alignment: Alignment.centerLeft,
                        child: Text(
                          'Día ${day.id} - ${day.toString()}',
                          style: TextStyle(fontSize: 24.0, fontFamily: "Bebas"),
                        ),
                      ),
                      trailing: Icon(
                        Icons.arrow_drop_down,
                        // Icons.swap_vertical_circle,
                        color: Utils.orange,
                        size: 32.0,
                      ),
                      children: days.map((day) {
                        return ListTile(
                          onTap: () {
                            setState(() {
                              expansionTileCollapse.currentState.collapse();
                              this.day = day;
                            });
                          },
                          selected: day.id == this.day.id,
                          title: Text(
                            'Día ${day.id} - ${day.toString()}',
                            style:
                                TextStyle(fontSize: 24.0, fontFamily: "Bebas"),
                          ),
                        );
                      }).toList(),
                    ),
                  ),
                ),
              ),
              Container(
                margin: EdgeInsets.symmetric(horizontal: 15.0, vertical: 10.0),
                child: Text(day.desc,
                    style: TextStyle(
                        fontSize: 16.0,
                        color: Colors.black,
                        fontWeight: FontWeight.w500)),
              ),
              Expanded(
                child: loading == true
                    ? LoadingLayout("¡Cargando películas!")
                    : SingleChildScrollView(
                        child: Column(
                          children: _dateTimeList.map((time) {
                            return Row(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Material(
                                  elevation: 2.0,
                                  child: Container(
                                    padding: EdgeInsets.all(6.0),
                                    color: Colors.white,
                                    child: Text(DateFormat('HH:mm', 'es_AR')
                                        .format(time)),
                                  ),
                                ),
                                _gridByHour(time)
                              ],
                            );
                          }).toList(),
                        ),
                      ),
              )
            ],
          ),
        ));
  }
}

class MovieTile extends StatelessWidget {
  final Movie movie;

  MovieTile({this.movie});

  void _pushMovieDetail(BuildContext context, Movie movie) {
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => MovieDetailPage(movie),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return InkWell(
        onTap: () {
          _pushMovieDetail(context, movie);
        },
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            SizedBox(
              height: 80.0,
              width: 200.0,
              child: Hero(
                tag: movie.thumbnail,
                child: Card(
                  elevation: 2.0,
                  clipBehavior: Clip.antiAlias,
                  color: Colors.transparent,
                  child: Stack(
                    fit: StackFit.expand,
                    alignment: AlignmentDirectional.center,
                    children: <Widget>[
                      CachedNetworkImage(
                          fit: BoxFit.cover,
                          imageUrl: movie.thumbnail,
                          placeholder: (context, url) => Image.asset(
                                'assets/placeholder_movie.png',
                                fit: BoxFit.cover,
                              ),
                          errorWidget: (context, url, error) =>
                              Icon(LineAwesomeIcons.exclamation_circle)),
                      if (movie.trailer.length != 0)
                        Center(
                          child: CircleAvatar(
                            backgroundColor: Colors.black.withOpacity(.65),
                            radius: 16.0,
                            child: Icon(
                              Icons.play_arrow,
                              color: Colors.white,
                              size: 26.0,
                            ),
                          ),
                        )
                      else
                        Container()
                    ],
                  ),
                ),
              ),
            ),
            Padding(
                padding: EdgeInsets.symmetric(vertical: 2.0, horizontal: 12.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      movie.title,
                      style: TextStyle(
                          fontWeight: FontWeight.w500, fontSize: 18.0),
                      maxLines: 1,
                      overflow: TextOverflow.ellipsis,
                    ),
                    Text(
                      movie.extra,
                      style: TextStyle(
                          color: Colors.black.withOpacity(.65), fontSize: 12.0),
                      maxLines: 1,
                      overflow: TextOverflow.ellipsis,
                    ),
                    // Text(
                    //   movie.toStringDate,
                    //   style: TextStyle(fontSize: 12.0),
                    // ),
                  ],
                )),
          ],
        ));
  }
}
