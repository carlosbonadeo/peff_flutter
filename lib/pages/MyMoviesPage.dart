import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:line_awesome_icons/line_awesome_icons.dart';
import 'package:path_provider/path_provider.dart';
import 'package:peff_flutter/data/Utils.dart';
import 'package:peff_flutter/models/Movie.dart';
import 'package:peff_flutter/pages/MovieDetailPage.dart';

class MyMoviesPage extends StatefulWidget {
  @override
  MyMoviesPageState createState() {
    return new MyMoviesPageState();
  }
}

class MyMoviesPageState extends State<MyMoviesPage> {
  List<Movie> myMovies = List();
  bool loading = true;

  Future<void> _loadMyMovies() async {
    Directory dir = await getApplicationDocumentsDirectory();
    File myMoviesFile = File("${dir.path}/myMovies.json");
    myMovies = movieFromJson(myMoviesFile.readAsStringSync());
    myMovies.sort((a, b) {
      if (a.day == b.day) return a.startDate.compareTo(b.startDate);

      return a.day.compareTo(b.day);
    });

    setState(() {
      loading = false;
    });
  }

  // Future<void> _deleteAllMovies() async {
  //   FlutterLocalNotificationsPlugin notification =
  //       new FlutterLocalNotificationsPlugin();
  //   Directory dir = await getApplicationDocumentsDirectory();
  //   File myMoviesFile = File("${dir.path}/myMovies.json");
  //   myMoviesFile.createSync();
  //   myMoviesFile.writeAsStringSync("[]");

  //   myMovies.forEach((movie) {
  //     notification.cancel(int.parse("10${movie.id}"));
  //   });

  //   setState(() {
  //     myMovies.clear();
  //   });
  // }

  @override
  void initState() {
    super.initState();
    _loadMyMovies();
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: !loading
          ? Column(
              mainAxisSize: MainAxisSize.max,
              children: <Widget>[
                Container(
                  padding: EdgeInsets.all(16.0),
                  child: Text(
                    "Estos son los eventos que te interesaron. Por eso queremos avisarte media hora antes de que empiecen así no te los perdés.",
                    style: TextStyle(fontSize: 16.0),
                  ),
                ),
                // Divider(),
                MyMoviesList(myMovies)
              ],
            )
          : Container(
              height: 250.0,
              alignment: Alignment.center,
              child: CircularProgressIndicator(),
            ),
    );
  }
}

class MyMoviesList extends StatefulWidget {
  final List<Movie> myMovies;

  MyMoviesList(this.myMovies);

  @override
  _MyMoviesListState createState() => _MyMoviesListState();
}

class _MyMoviesListState extends State<MyMoviesList> {
  List<Movie> myMovies;

  Future _removeMovie(Movie movie) async {
    FlutterLocalNotificationsPlugin notification =
        new FlutterLocalNotificationsPlugin();

    Directory dir = await getApplicationDocumentsDirectory();
    File myMoviesFile = File("${dir.path}/myMovies.json");

    setState(() {
      myMovies.removeWhere((movieObj) => movieObj.id == movie.id);
    });

    myMoviesFile.createSync();
    myMoviesFile.writeAsStringSync(movieToJson(myMovies));

    notification.cancel(int.parse("10${movie.id}"));
  }

  Future _pushMovieDetail(BuildContext context, Movie movie) async {
    await Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => MovieDetailPage(movie),
      ),
    );

    Directory dir = await getApplicationDocumentsDirectory();
    File myMoviesFile = File("${dir.path}/myMovies.json");

    setState(() {
      myMovies = movieFromJson(myMoviesFile.readAsStringSync());
    });
  }

  @override
  void initState() {
    super.initState();
    myMovies = widget.myMovies;
  }

  @override
  Widget build(BuildContext context) {
    if (myMovies.length == 0)
      return Container(
          height: 260.0,
          alignment: Alignment.center,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Icon(
                LineAwesomeIcons.heartbeat,
                size: 80.0,
                color: Colors.grey,
              ),
              Padding(
                padding: EdgeInsets.only(top: 20.0),
                child: Text(
                  "Sin películas favoritas todavía",
                  style: TextStyle(color: Colors.grey),
                ),
              )
            ],
          ));
    else
      return ListView.builder(
        shrinkWrap: true,
        primary: false,
        itemCount: myMovies.length,
        itemBuilder: (context, i) {
          Movie movie = myMovies[i];

          return ListTile(
            onTap: () {
              _pushMovieDetail(context, movie);
            },
            leading: Stack(
              alignment: AlignmentDirectional.center,
              children: <Widget>[
                Container(
                  width: 1.0,
                  height: 72.0,
                  color: Colors.black26,
                ),
                Container(
                  width: 20.0,
                  height: 20.0,
                  decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    color: Utils.primaryColor,
                    border: Border.all(color: Colors.white, width: 3.0),
                  ),
                )
              ],
            ),
            title: Text(movie.title),
            subtitle: Text("Día ${movie.day} - ${movie.toStringDate}"),
            trailing: IconButton(
              icon: Icon(LineAwesomeIcons.times),
              color: Colors.red,
              onPressed: () {
                _removeMovie(movie);
              },
            ),
          );
        },
      );
  }
}
