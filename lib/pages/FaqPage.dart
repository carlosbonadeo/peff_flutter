import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

class FaqPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Material(
      child: new ListView(
        children: <Widget>[
          Container(
            margin: EdgeInsets.all(26.0),
            child: Image.asset(
              "assets/logo.png",
              height: 130.0,
            ),
          ),
          Text(
            "Patagonia Eco Film Fest",
            textAlign: TextAlign.center,
            style: TextStyle(
              fontSize: 28.0,
              fontFamily: "Bebas",
            ),
          ),
          SizedBox(
            height: 15.0,
          ),
          Container(
            margin: EdgeInsets.all(16.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Text("4ta edición - 2 al 5 de octubre de 2019", textScaleFactor: 1.2,),
                Text("Puerto Madryn - Chubut", textScaleFactor: 1.2,),
                Text("Entrada libre y gratuita", textScaleFactor: 1.2,),
                InkWell(
                  child: Text(
                    "http://www.patagoniaecofilmfest.com",
                    style: TextStyle(
                        fontSize: 16.0,
                        color: Colors.blue,
                        decoration: TextDecoration.underline),
                  ),
                  onTap: () async {
                    const url = "http://www.patagoniaecofilmfest.com";
                    if (await canLaunch(url))
                      await launch(url);
                    else
                      throw 'No se puede acceder a $url';
                  },
                )
              ],
            ),
          ),
          InfoTile("¿Dónde se realizará?",
              "El festival cuenta con 3 sedes donde se llevarán a cabo las diferentes proyecciones y actividades especiales.\nEl Cine TEatro Auditorium, el Auditorio Ecocentro y el Teatro del Muelles.\nConsultá el programa oficial para más información."),
          InfoTile("¿Quién puede participar?",
              "El festival es abierto a la comunidad y todas las proyecciones y actividades son con entrada libre y gratuita."),
        ],
      ),
    );
  }
}

class InfoTile extends StatelessWidget {
  final String question;
  final String answer;
  final bool isExpanded;

  InfoTile(this.question, this.answer, [this.isExpanded = false]);

  @override
  Widget build(BuildContext context) {
    return ExpansionTile(
      title: new Text(question),
      initiallyExpanded: isExpanded,
      children: <Widget>[
        new Padding(
            padding: new EdgeInsets.all(16.0),
            child: new Text(answer, textScaleFactor: 1.2))
      ],
    );
  }
}
