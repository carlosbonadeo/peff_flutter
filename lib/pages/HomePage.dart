import 'package:flutter/material.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:peff_flutter/components/BackdropMenu.dart';
import 'package:backdrop/backdrop.dart';

class HomePage extends StatefulWidget {
  final int current;
  final Widget page;

  HomePage({this.current = 0, this.page});

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage>
    with SingleTickerProviderStateMixin {
  FlutterLocalNotificationsPlugin notification =
      new FlutterLocalNotificationsPlugin();
  AnimationController backdropController;

  @override
  void initState() {
    super.initState();

    backdropController = AnimationController(
        vsync: this, duration: Duration(milliseconds: 100), value: 1.0);
  }

  @override
  Widget build(BuildContext context) {
    return BackdropScaffold(
      controller: backdropController,
      title: Text("Pagatonia Eco Film Fest"),
      backLayer: BackdropMenu(
        current: widget.current,
        onItemTap: () => backdropController.fling(),
      ),
      frontLayer: widget.page,
      frontLayerBorderRadius: BorderRadius.all(Radius.circular(0.0)),
      // frontLayer: ClipRRect(
      //   borderRadius: BorderRadius.only(
      //         topLeft: Radius.circular(16.0), topRight: Radius.circular(16.0)),
      //   child: widget.page),
    );
  }
}
