import 'dart:math';

import 'package:flutter/material.dart';
import 'package:peff_flutter/data/Utils.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

class LocationTab extends StatefulWidget {
  @override
  LocationTabState createState() {
    return new LocationTabState();
  }
}

class LocationTabState extends State<LocationTab> {
  GoogleMapController mapController;
  List<Sede> sedes = [
    Sede("Cine Teatro Auditorium", "28 de Julio 129, Puerto Madryn, Chubut",
        "sede01", LatLng(-42.766070, -65.035418)),
    Sede("Auditorio Ecocentro", "Julio Verne 3784, Puerto Madryn, Chubut",
        "sede02", LatLng(-42.782243, -64.997282)),
    Sede("Teatro del Muelle", "Av. Guillermo Rawson 60, Puerto Madryn, Chubut",
        "sede03", LatLng(-42.762821, -65.035285)),
  ];
  Sede activeLocation;
  String _mapStyle =
      '[{"elementType": "geometry","stylers": [{"color": "#ebe3cd"}]},{"elementType": "labels.text.fill","stylers": [{"color": "#523735"}]},{"elementType": "labels.text.stroke","stylers": [{"color": "#f5f1e6"}]},{"featureType": "administrative","elementType": "geometry.stroke","stylers": [{"color": "#c9b2a6"}]},{"featureType": "administrative.land_parcel","elementType": "geometry.stroke","stylers": [{"color": "#dcd2be"}]},{"featureType": "administrative.land_parcel","elementType": "labels.text.fill","stylers": [{"color": "#ae9e90"}]},{"featureType": "landscape.natural","elementType": "geometry","stylers": [{"color": "#dfd2ae"}]},{"featureType": "poi","elementType": "geometry","stylers": [{"color": "#dfd2ae"}]},{"featureType": "poi","elementType": "labels.text.fill","stylers": [{"color": "#93817c"}]},{"featureType": "poi.park","elementType": "geometry.fill","stylers": [{"color": "#a5b076"}]},{"featureType": "poi.park","elementType": "labels.text.fill","stylers": [{"color": "#447530"}]},{"featureType": "road","elementType": "geometry","stylers": [{"color": "#f5f1e6"}]},{"featureType": "road.arterial","elementType": "geometry","stylers": [{"color": "#fdfcf8"}]},{"featureType": "road.highway","elementType": "geometry","stylers": [{"color": "#f8c967"}]},{"featureType": "road.highway","elementType": "geometry.stroke","stylers": [{"color": "#e9bc62"}]},{"featureType": "road.highway.controlled_access","elementType": "geometry","stylers": [{"color": "#e98d58"}]},{"featureType": "road.highway.controlled_access","elementType": "geometry.stroke","stylers": [{"color": "#db8555"}]},{"featureType": "road.local","elementType": "labels.text.fill","stylers": [{"color": "#806b63"}]},{"featureType": "transit.line","elementType": "geometry","stylers": [{"color": "#dfd2ae"}]},{"featureType": "transit.line","elementType": "labels.text.fill","stylers": [{"color": "#8f7d77"}]},{"featureType": "transit.line","elementType": "labels.text.stroke","stylers": [{"color": "#ebe3cd"}]},{"featureType": "transit.station","elementType": "geometry","stylers": [{"color": "#dfd2ae"}]},{"featureType": "water","elementType": "geometry.fill","stylers": [{"color": "#b9d3c2"}]},{"featureType": "water","elementType": "labels.text.fill","stylers": [{"color": "#92998d"}]}]';

  void _onMapCreated(GoogleMapController controller) {
    mapController = controller;
    mapController.setMapStyle(_mapStyle);
  }

  _centerMap() {
    setState(() {
      activeLocation = null;
    });

    double minLat = sedes[0].location.latitude ?? 0.0;
    double minLong = sedes[0].location.longitude ?? 0.0;
    double maxLat = sedes[0].location.latitude ?? 0.0;
    double maxLong = sedes[0].location.longitude ?? 0.0;

    for (Sede sede in sedes) {
      minLat = min(minLat, sede.location.latitude);
      minLong = min(minLong, sede.location.longitude);

      maxLat = max(maxLat, sede.location.latitude);
      maxLong = max(maxLong, sede.location.longitude);
    }

    mapController.animateCamera(
      CameraUpdate.newLatLngBounds(
        LatLngBounds(
          southwest: LatLng(minLat, minLong),
          northeast: LatLng(maxLat, maxLong),
        ),
        60.0,
      ),
    );
  }

  void _selectLocation(Sede sede) {
    setState(() {
      activeLocation = sede;
    });
  }

  Set<Marker> _buildMarkers() {
    return sedes
        .map(
          (sede) => Marker(
            markerId: MarkerId(sede.label),
            position: sede.location,
            icon: BitmapDescriptor.fromAsset("assets/marker.png"),
            onTap: () {
              _selectLocation(sede);
            },
          ),
        )
        .toSet();
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      alignment: AlignmentDirectional.bottomCenter,
      children: <Widget>[
        GoogleMap(
          onMapCreated: _onMapCreated,
          // cameraTargetBounds: CameraTargetBounds(LatLngBounds(southwest: sedes[1].location,
          // northeast: sedes[0].location,)),
          initialCameraPosition:
              CameraPosition(target: LatLng(-42.774666, -65.018159), zoom: 13),
          markers: _buildMarkers(),
          compassEnabled: true,
          onTap: (LatLng latLng) {
            setState(() {
              activeLocation = null;
            });
          },
        ),
        Positioned(
          top: 10,
          left: 10,
          right: 10,
          child: Card(
            clipBehavior: Clip.antiAlias,
            child: activeLocation == null
                ? Padding(
                    child: Text("¡Tocá en una sede para ver como llegar!", textAlign: TextAlign.center,),
                    padding: EdgeInsets.all(10.0),
                  )
                : Row(
                    children: <Widget>[
                      SizedBox(
                          height: 100.0,
                          width: 130.0,
                          child: FadeInImage.assetNetwork(
                            placeholder: 'assets/placeholder.jpg',
                            image: activeLocation.imgPath,
                            fit: BoxFit.fill,
                          )),
                      Expanded(
                        child: Padding(
                          padding: EdgeInsets.symmetric(horizontal: 8.0),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Text(
                                activeLocation.label,
                                softWrap: true,
                                style: TextStyle(
                                    fontWeight: FontWeight.w500,
                                    fontSize: 17.0),
                              ),
                              Text(
                                activeLocation.address,
                                style: TextStyle(fontSize: 15.0),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
          ),
        ),
        Positioned(
          bottom: 15.0,
          left: 20.0,
          child: FloatingActionButton(
            heroTag: "centerMap",
            mini: true,
            child: Icon(Icons.fullscreen),
            onPressed: _centerMap,
            backgroundColor: Utils.orange,
            foregroundColor: Colors.white,
          ),
        ),
      ],
    );
  }
}

class Sede {
  String label;
  String address;
  String img;
  LatLng location;

  Sede(this.label, this.address, this.img, this.location);

  String get imgPath {
    return "http://www.patagoniaecofilmfest.com/peff/img/sedes/$img.jpg";
  }
}
