import 'dart:io';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:line_awesome_icons/line_awesome_icons.dart';
import 'package:path_provider/path_provider.dart';
import 'package:peff_flutter/data/Utils.dart';
import 'package:peff_flutter/models/Movie.dart';
import 'package:share/share.dart';
import 'package:sliver_fab/sliver_fab.dart';
import 'package:url_launcher/url_launcher.dart';

class MovieDetailPage extends StatefulWidget {
  final Movie movie;

  MovieDetailPage(this.movie);

  @override
  _MovieDetailPageState createState() => _MovieDetailPageState();
}

class _MovieDetailPageState extends State<MovieDetailPage> {
  bool isInMyMovies = false;
  FlutterLocalNotificationsPlugin notification =
      new FlutterLocalNotificationsPlugin();

  @override
  void initState() {
    super.initState();
    _checkMyMovies();
  }

  List<Widget> _chipsBuilder(String chips) {
    return chips
        .split(" / ")
        .map((item) => Container(
              margin: EdgeInsets.only(right: 6.0),
              child: Chip(
                label: Text(item, textScaleFactor: .9),
              ),
            ))
        .toList();
  }

  Future<List<Movie>> _getMyMoviesFile() async {
    Directory dir = await getApplicationDocumentsDirectory();
    File myMoviesFile = File("${dir.path}/myMovies.json");

    return movieFromJson(myMoviesFile.readAsStringSync());
  }

  Future<void> _checkMyMovies() async {
    List<Movie> myMovies = await _getMyMoviesFile();

    myMovies.forEach((movie) {
      if (this.widget.movie.id == movie.id) {
        setState(() {
          isInMyMovies = true;
        });
        return;
      }
    });
  }

  Future<void> _addMovie(Movie movie) async {
    List<Movie> myMovies = await _getMyMoviesFile();

    Directory dir = await getApplicationDocumentsDirectory();
    File myMoviesFile = File("${dir.path}/myMovies.json");

    myMovies.add(movie);

    myMoviesFile.createSync();
    myMoviesFile.writeAsStringSync(movieToJson(myMovies));

    setState(() {
      isInMyMovies = true;
    });

    var android = new AndroidNotificationDetails('1', '2', '3',
        importance: Importance.Max,
        priority: Priority.High,
        color: Utils.primaryColor);
    var ios = new IOSNotificationDetails();
    var settings = new NotificationDetails(android, ios);
    DateTime halfHourBeforeMovie =
        DateTime.parse(movie.startDate).subtract(Duration(minutes: 30));

    notification.schedule(
      int.parse("10${movie.id}"),
      'Patagonia Eco Film Fest',
      'En 30 minutos comienza "${movie.title}", ¡no te la pierdas!',
      halfHourBeforeMovie,
      settings,
    );
  }

  Future<void> _removeMovie(Movie movie) async {
    List<Movie> myMovies = await _getMyMoviesFile();

    Directory dir = await getApplicationDocumentsDirectory();
    File myMoviesFile = File("${dir.path}/myMovies.json");

    myMovies.removeWhere((movieObj) => movieObj.id == movie.id);

    myMoviesFile.createSync();
    myMoviesFile.writeAsStringSync(movieToJson(myMovies));

    setState(() {
      isInMyMovies = false;
    });

    notification.cancel(int.parse("10${movie.id}"));
  }

  _shareMovie() {
    String shareText =
        "¿Vamos a ver \"${widget.movie.title}\" en el Patagonia Eco Film Fest?";

    if (widget.movie.trailer.length != 0)
      shareText += " Mirá ${widget.movie.trailer}";

    print(shareText);

    Share.share(shareText);
  }

  List<TableRow> _buildRows() {
    List<TableRow> rows = List();

    rows.add(
      TableRow(children: <Widget>[Text("Título"), Text(widget.movie.title)]),  
    );

    rows.add(TableRow(children: <Widget>[Text("Sala"), Text(widget.movie.location)]));

    if (widget.movie.director.isNotEmpty)
      rows.add(TableRow(
          children: <Widget>[Text("Director"), Text(widget.movie.director)]));

    if (widget.movie.country.isNotEmpty)
      rows.add(TableRow(
          children: <Widget>[Text("Origen"), Text(widget.movie.country)]));

    return rows;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // bottomNavigationBar: MovieFooter(widget.movie),
      bottomNavigationBar: Container(
          padding: EdgeInsets.only(bottom: 8.0, top: 12.0),
          height: 60.0,
          // height: !movie.isEnded ? 60.0 : 90.0,
          color: Utils.primaryColor,
          alignment: Alignment.center,
          child: Text(
            "${widget.movie.toStringDate.toUpperCase()}",
            style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18.0),
          )),
      body: SliverFab(
        floatingWidget: widget.movie.trailer.length != 0
            ? FloatingActionButton(
                onPressed: () async {
                  String url = widget.movie.trailer;
                  if (await canLaunch(url))
                    await launch(url);
                  else
                    throw 'No se puede acceder a $url';
                },
                heroTag: widget.movie.synopsis,
                backgroundColor: Colors.red,
                foregroundColor: Colors.white,
                child: Icon(
                  Icons.play_arrow,
                  size: 36.0,
                ))
            : Container(),
        slivers: <Widget>[
          SliverAppBar(
            expandedHeight: 256.0,
            pinned: true,
            iconTheme: IconThemeData(color: Colors.white),
            actions: <Widget>[
              IconButton(
                icon:
                    Icon(isInMyMovies ? Icons.favorite : Icons.favorite_border),
                onPressed: () {
                  if (isInMyMovies)
                    _removeMovie(widget.movie);
                  else
                    _addMovie(widget.movie);
                },
              ),
              IconButton(icon: Icon(Icons.share), onPressed: _shareMovie),
            ],
            flexibleSpace: FlexibleSpaceBar(
              title: Padding(
                padding: EdgeInsets.only(right: 110.0),
                child: Text(
                  widget.movie.title,
                  style: TextStyle(color: Colors.white),
                  overflow: TextOverflow.ellipsis,
                  maxLines: 1,
                ),
              ),
              background: Stack(
                fit: StackFit.expand,
                children: <Widget>[
                  Hero(
                    tag: widget.movie.thumbnail,
                    child: CachedNetworkImage(
                      fit: BoxFit.cover,
                      imageUrl: widget.movie.cover,
                      placeholder: (context, url) => Image.asset(
                        'assets/placeholder_movie.png',
                        fit: BoxFit.fill,
                      ),
                      errorWidget: (context, url, error) =>
                          Icon(LineAwesomeIcons.exclamation_circle),
                    ),
                  ),
                  DecoratedBox(
                    decoration: BoxDecoration(
                      gradient: LinearGradient(
                        begin: Alignment(0.0, -1.0),
                        end: Alignment(0.0, -0.4),
                        colors: <Color>[Color(0x60000000), Color(0x00000000)],
                      ),
                    ),
                  )
                ],
              ),
            ),
          ),
          SliverList(
            delegate: SliverChildListDelegate(<Widget>[
              Padding(
                padding: EdgeInsets.fromLTRB(16.0, 16.0, 16.0, 0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Row(children: _chipsBuilder(widget.movie.extra)),
                    SizedBox(
                      height: 12.0,
                    ),
                    Table(
                        columnWidths: {0: FractionColumnWidth(.25)},
                        children: _buildRows()),
                  ],
                ),
              ),
              Padding(
                  padding: EdgeInsets.all(16.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text("Sinopsis",
                          textScaleFactor: 1.3,
                          style: TextStyle(color: Colors.black45)),
                      Text(widget.movie.synopsis,
                          textScaleFactor: 1.1, style: TextStyle(height: 1.1)),
                    ],
                  ))
            ]),
          )
        ],
      ),
    );
  }
}

class MovieFooter extends StatefulWidget {
  final Movie movie;

  MovieFooter(this.movie);

  @override
  _MovieFooterState createState() => _MovieFooterState();
}

class _MovieFooterState extends State<MovieFooter> {
  Movie movie;
  bool isInMyMovies = false;
  FlutterLocalNotificationsPlugin notification =
      new FlutterLocalNotificationsPlugin();

  Future<List<Movie>> _getMyMoviesFile() async {
    Directory dir = await getApplicationDocumentsDirectory();
    File myMoviesFile = File("${dir.path}/myMovies.json");

    return movieFromJson(myMoviesFile.readAsStringSync());
  }

  Future<void> _checkMyMovies() async {
    List<Movie> myMovies = await _getMyMoviesFile();

    myMovies.forEach((movie) {
      if (this.movie.id == movie.id) {
        setState(() {
          isInMyMovies = true;
        });
        return;
      }
    });
  }

  Future<void> _addMovie(Movie movie) async {
    List<Movie> myMovies = await _getMyMoviesFile();

    Directory dir = await getApplicationDocumentsDirectory();
    File myMoviesFile = File("${dir.path}/myMovies.json");

    myMovies.add(movie);

    myMoviesFile.createSync();
    myMoviesFile.writeAsStringSync(movieToJson(myMovies));

    setState(() {
      isInMyMovies = true;
    });

    var android = new AndroidNotificationDetails('1', '2', '3',
        importance: Importance.Max,
        priority: Priority.High,
        color: Utils.primaryColor);
    var ios = new IOSNotificationDetails();
    var settings = new NotificationDetails(android, ios);
    DateTime halfHourBeforeMovie =
        DateTime.parse(movie.startDate).subtract(Duration(minutes: 30));

    notification.schedule(
      int.parse("10${movie.id}"),
      'Patagonia Eco Film Fest',
      'En 30 minutos comienza "${movie.title}", ¡no te la pierdas!',
      halfHourBeforeMovie,
      settings,
    );
  }

  Future<void> _removeMovie(Movie movie) async {
    List<Movie> myMovies = await _getMyMoviesFile();

    Directory dir = await getApplicationDocumentsDirectory();
    File myMoviesFile = File("${dir.path}/myMovies.json");

    myMovies.removeWhere((movieObj) => movieObj.id == movie.id);

    myMoviesFile.createSync();
    myMoviesFile.writeAsStringSync(movieToJson(myMovies));

    setState(() {
      isInMyMovies = false;
    });

    notification.cancel(int.parse("10${movie.id}"));
  }

  Widget _buildMovieFooter() {
    if (isInMyMovies)
      return Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(
            "${movie.toStringDate.toUpperCase()}",
            style: TextStyle(fontWeight: FontWeight.bold, fontSize: 15.0),
          ),
          IconButton(
            color: Colors.red[400],
            icon: Icon(
              LineAwesomeIcons.heart,
              size: 34.0,
            ),
            onPressed: () {
              _removeMovie(movie);
            },
          ),
        ],
      );
    else
      return Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Text(
            "${movie.toStringDate.toUpperCase()}",
            style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18.0),
          ),
          IconButton(
            icon: Icon(LineAwesomeIcons.heart_o),
            onPressed: () {
              _addMovie(movie);
            },
          ),
        ],
      );
  }

  @override
  void initState() {
    super.initState();
    _checkMyMovies();
    movie = widget.movie;
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        padding: EdgeInsets.only(bottom: 8.0, top: 12.0),
        height: 60.0,
        color: Utils.primaryColor,
        alignment: Alignment.center,
        child: _buildMovieFooter());
  }
}
