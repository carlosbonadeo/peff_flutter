import 'package:flutter/material.dart';
import 'package:line_awesome_icons/line_awesome_icons.dart';
import 'package:peff_flutter/data/Utils.dart';
import 'package:peff_flutter/models/Sponsor.dart';
import 'package:cached_network_image/cached_network_image.dart';

class SponsorsPage extends StatefulWidget {
  @override
  SponsorsPageState createState() {
    return new SponsorsPageState();
  }
}

class SponsorsPageState extends State<SponsorsPage> {
  @override
  Widget build(BuildContext context) {
    return Material(
        color: Color(0xFF8DC9A5),
        child: ListView(
          padding: EdgeInsets.all(8.0),
          children: <Widget>[
            Container(
              padding: EdgeInsets.all(8.0),
              child: Text(
                "El festival es posible gracias al apoyo de todas estas organizaciones",
                style: TextStyle(fontSize: 16.0),
              ),
            ),
            Item("Organizan", Utils.organizan),
            Item("Auspician", Utils.sponsors),
            Item("Colaboran", Utils.colaboran),
            Item("Media-Partners", Utils.mediaPartners),
            Item("Nos apoyan", Utils.nosApoyan),
          ],
        ));
  }
}

class Item extends StatelessWidget {
  final String title;
  final List<Sponsor> sponsors;

  Item(this.title, this.sponsors);

  @override
  Widget build(BuildContext context) {
    return Card(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Container(
            child: Text(
              title,
              style: TextStyle(fontSize: 24.0, fontFamily: "Bebas"),
            ),
            padding: EdgeInsets.all(12.0),
          ),
          Container(
            alignment: Alignment.center,
            padding: const EdgeInsets.all(8.0),
            child: Wrap(
              alignment: WrapAlignment.center,
              crossAxisAlignment: WrapCrossAlignment.center,
              children: sponsors.map((sponsor) {
                return Container(
                  width: 80.0,
                  height: 80.0,
                  margin: EdgeInsets.all(6.0),
                  child: CachedNetworkImage(
                    imageUrl: sponsor.imgPath,
                    placeholder: (context, url) => Image.asset('assets/placeholder_white.png'),
                    errorWidget: (context, url, error) => Icon(LineAwesomeIcons.exclamation_circle)
                  ),
                );
              }).toList(),
            ),
          ),
        ],
      ),
    );
  }
}
