import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:peff_flutter/data/Utils.dart';
import 'package:peff_flutter/models/Staff.dart';

class StaffPage extends StatelessWidget {
  final List<Staff> staff = [
    Staff("Cristian Pérez Scigliano", "Director / Productor", "portrait-1.jpg"),
    Staff("Joel Hume", "Licenciado en Ciencias del Ambiente / Director de 4R",
        "portrait-2.jpg"),
    Staff(
        "Alejandra Scigliano",
        "Periodista especializada en RSE y Sustentabilidad. Dirige Unirse",
        "portrait-3.jpg"),
    Staff("Rocío Pérez", "Diseño gráfico", "rocioperez.jpg"),
    Staff("Marcos Jamsech", "Diseño web", "marcos.jpg"),
    Staff("Glynnis Ritter", "Directora de programación", "glynnis.jpg"),
  ];

  final List<String> colaboradores = [
    "Luis Eduardo Flores",
    "Sebastián López García",
    "Jade Zoel Téllez Hernández",
    "Camila Vega",
    "David Alexander Choque",
    "Emanuel Pistara",
    "Bianca Giuggia Abrany",
    "Marcela Bianotti",
    "Marcos Antiman",
    "Lucas Pérez Scigliano",
    "Alberto Pérez",
    "Carlos Bonadeo",
  ];

  @override
  Widget build(BuildContext context) {
    return Material(
      child: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Padding(
              padding: EdgeInsets.all(16.0),
              child: RichText(
                text: TextSpan(
                    style: TextStyle(color: Colors.black, fontSize: 16.0),
                    children: [
                      TextSpan(text: "El "),
                      TextSpan(
                          text:
                              "Patagonia Eco Film Fest - Festival Internacional de Cine Ambiental",
                          style: TextStyle(fontWeight: FontWeight.bold)),
                      TextSpan(
                          text:
                              " se consolida como una de las propuestas ambientales y culturales más importantes de la Argentina. En su 4ta edición la cual se llevará a cabo del "),
                      TextSpan(
                          text: "2 al 5 de octubre de 2019",
                          style: TextStyle(fontWeight: FontWeight.bold)),
                      TextSpan(
                          text:
                              " en la ciudad de Puerto Madryn, se proyectarán más de 50 películas largometrajes y cortometrajes de temática ambiental a nivel internacional en diferentes puntos de la ciudad, con "),
                      TextSpan(
                          text: "entrada libre y gratuita.",
                          style: TextStyle(fontWeight: FontWeight.bold)),
                    ]),
              ),
            ),
            Padding(
                padding: EdgeInsets.all(16.0),
                child: Text(
                    "Con una mirada integradora, crítica y optimista, las producciones audiovisuales incluidas en el festival, proponen informar, concientizar y contribuir a mejorar la calidad de vida de nuestra comunidad y nuestro planeta.",
                    textScaleFactor: 1.1)),
            Padding(
              padding: new EdgeInsets.all(16.0),
              child: Text(
                "Nosotros",
                style: TextStyle(fontSize: 28.0, fontFamily: "Bebas"),
              ),
            ),
            GridView.count(
              crossAxisCount: 3,
              shrinkWrap: true,
              primary: false,
              crossAxisSpacing: 20.0,
              mainAxisSpacing: 20.0,
              padding: EdgeInsets.symmetric(horizontal: 24.0),
              children: staff.map((item) {
                return InkWell(
                  onTap: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => StaffDetail(item)));
                  },
                  child: Hero(
                    child: CircleAvatar(
                        radius: 80.0,
                        backgroundImage: NetworkImage(item.imgPath)),
                    tag: item.img,
                  ),
                );
              }).toList(),
            ),
            Padding(
              padding: new EdgeInsets.only(top: 32.0, left: 16.0, bottom: 13.0),
              child: Text(
                "Colaboradores",
                style: TextStyle(fontSize: 28.0, fontFamily: "Bebas"),
              ),
            ),
            Center(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: colaboradores.map((colaborador) {
                  return Padding(
                    padding: EdgeInsets.only(left: 16.0, bottom: 16.0),
                    child: Text(colaborador),
                  );
                }).toList(),
              ),
            )
          ],
        ),
      ),
    );
  }
}

class StaffDetail extends StatelessWidget {
  final Staff item;

  StaffDetail(this.item);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Utils.primaryColor,
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        elevation: 0,
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Hero(
              tag: item.img,
              child: CircleAvatar(
                backgroundImage: NetworkImage(item.imgPath),
                radius: 100.0,
              ),
            ),
            SizedBox(
              height: 10.0,
            ),
            Text(
              item.name,
              textScaleFactor: 1.4,
              style: new TextStyle(height: 1.4, fontWeight: FontWeight.bold),
              textAlign: TextAlign.center,
            ),
            Container(
              width: 200.0,
              padding: EdgeInsets.only(top: 8.0),
              child: Text(
                item.role,
                style: new TextStyle(height: 1.1),
                textAlign: TextAlign.center,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
