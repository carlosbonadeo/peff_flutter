import 'dart:async';

//import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
//import 'package:line_awesome_icons/line_awesome_icons.dart';
import 'package:peff_flutter/data/Utils.dart';
import 'package:peff_flutter/models/Day.dart';
import 'package:peff_flutter/pages/HomePage.dart';
import 'package:peff_flutter/pages/MoviesPage.dart';

class CarrouselPage extends StatefulWidget {
  final int initialPage;

  CarrouselPage({this.initialPage = 0});

  @override
  _CarrouselPageState createState() => _CarrouselPageState();
}

class _CarrouselPageState extends State<CarrouselPage> {
  PageController controller;
  double page = 0;
  List<Day> days;
  Timer timer;

  @override
  void initState() {
    super.initState();
    days = Utils.dayList;

    controller = PageController(initialPage: widget.initialPage);

    timer = Timer.periodic(Duration(seconds: 6), (Timer t) {
      // if (controller.page != 3)

      // print("1. controller.page: ${controller.page}");

      if (controller.hasClients) {
        if (controller.page == 3)
          controller.animateToPage(0,
              duration: Duration(milliseconds: 250), curve: Curves.easeOut);
        else
          controller
              .nextPage(
                  duration: Duration(milliseconds: 500),
                  curve: Curves.elasticOut)
              .then((onValue) {
            // print("2. controller.page: ${controller.page}");
          });

        setState(() {
          page = .25 * (controller.page + 1.0);
        });
      }
    });

    controller.addListener(() {
      setState(() {
        page = .25 * (controller.page + 1.0);
      });
    });
  }

  @override
  void dispose() {
    timer?.cancel();
    controller?.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        image: DecorationImage(
          fit: BoxFit.cover,
          image: AssetImage('assets/flyer2019.png'),
        ),
      ),
      child: Stack(
        alignment: AlignmentDirectional.bottomCenter,
        children: <Widget>[
          // Container(height: 80.0,child: Text("HOLAAA")),
          PageView(
            controller: controller,
            children: days.map((day) => DayView(day)).toList(),
          ),
          LinearProgressIndicator(
            value: page,
          )
        ],
      ),
    );
  }
}

class DayView extends StatelessWidget {
  final Day day;

  DayView(this.day);

  void _selectDay(context, day) {
    Navigator.push(
        context,
        MaterialPageRoute(
          builder: (BuildContext context) => HomePage(
            current: 3,
            page: MoviesPage(day: day),
          ),
        ));
  }

  @override
  Widget build(BuildContext context) {
    // return Material(
    //   child: InkWell(
    //     onTap: () {
    //       _selectDay(context, day);
    //     },
    //     child:
    return Stack(
      fit: StackFit.expand,
      children: <Widget>[
        // CachedNetworkImage(
        //     fit: BoxFit.fitHeight,
        //     imageUrl:
        //         'http://www.patagoniaecofilmfest.com/peff/img/${day.img}.jpg',
        //     placeholder: (context, url) =>
        //         Image.asset('assets/placeholder_white.png'),
        //     errorWidget: (context, url, error) =>
        //         Icon(LineAwesomeIcons.exclamation_circle)),
        Positioned(
          bottom: 11.0,
          left: 5.0,
          right: 5.0,
          child: Material(
            color: Colors.transparent,
            child: InkWell(
              onTap: () {
                _selectDay(context, day);
              },
              child: Hero(
                tag: day.id,
                child: Card(
                  color: Utils.orange,
                  child: Container(
                    padding: EdgeInsets.all(10.0),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Expanded(
                            child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text(
                              "Día ${day.id} - ${day.toString()}",
                              style: TextStyle(
                                  fontFamily: "Bebas", fontSize: 30.0, color: Colors.white,)
                            ),
                            Text(
                              day.desc,
                              maxLines: 2,
                              overflow: TextOverflow.ellipsis,
                              style: TextStyle(
                                  color: Colors.white70, fontSize: 15.0),
                            )
                          ],
                        )),
                        // Icon(LineAwesomeIcons.arrow_circle_right,
                        //     size: 48.0, color: Color(0xFF77AA8B))
                      ],
                    ),
                  ),
                ),
              ),
            ),
          ),
        ),
        day.isToday
            ? Positioned(
                left: 15.0,
                bottom: 125.0,
                child: Chip(
                  shape: StadiumBorder(
                      side: BorderSide(color: Colors.white, width: 6.0)),
                  label: Container(
                    child: Text("HOY!"),
                    width: 60.0,
                    alignment: Alignment.center,
                  ),
                  labelStyle: TextStyle(
                      fontSize: 24.0, color: Colors.black, fontFamily: "Bebas"),
                  padding: EdgeInsets.all(8.0),
                  backgroundColor: Utils.orange,
                ),
              )
            : Container(),
      ],
    );
    //   ),
    // );
  }
}
