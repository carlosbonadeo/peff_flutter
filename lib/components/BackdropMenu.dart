import 'package:flutter/material.dart';
import 'package:line_awesome_icons/line_awesome_icons.dart';
// import 'package:peff_flutter/pages/CameraPage.dart';
import 'package:peff_flutter/pages/CarrouselPage.dart';
import 'package:peff_flutter/pages/FaqPage.dart';
import 'package:peff_flutter/pages/MyMoviesPage.dart';
import 'package:peff_flutter/pages/HomePage.dart';
import 'package:peff_flutter/pages/LocationTab.dart';
import 'package:peff_flutter/pages/MoviesPage.dart';
import 'package:peff_flutter/pages/SponsorsPage.dart';
import 'package:peff_flutter/pages/StaffPage.dart';
import 'package:url_launcher/url_launcher.dart';

class BackdropMenu extends StatelessWidget {
  final int current;
  final VoidCallback onItemTap;

  BackdropMenu({Key key, this.current = 1, this.onItemTap}) : super(key: key);

  final List<ItemMenu> menu = [
    ItemMenu(1, "Día x día", LineAwesomeIcons.calendar, CarrouselPage(),
        Display.backdrop),
    ItemMenu(
        2, "Sedes", LineAwesomeIcons.map_signs, LocationTab(), Display.backdrop),
    ItemMenu(
        3, "Cartelera", LineAwesomeIcons.film, MoviesPage(), Display.backdrop),
    // ItemMenu(4, "¡Whisky!", LineAwesomeIcons.camera, CameraPage(),
    //     Display.fullScreen),
    ItemMenu(8, "Mis pelis", LineAwesomeIcons.heartbeat, MyMoviesPage(),
        Display.backdrop),
    ItemMenu(
        5, "Nosotros", LineAwesomeIcons.smile_o, StaffPage(), Display.backdrop),
    ItemMenu(6, "Quiénes nos apoyan", LineAwesomeIcons.thumbs_up,
        SponsorsPage(), Display.backdrop),
    ItemMenu(
        7, "El festival", LineAwesomeIcons.tree, FaqPage(), Display.backdrop),
  ];

  final List<Social> social = [
    Social("Facebook", "face.png",
        "https://www.facebook.com/patagoniaecofilmfest/"),
    Social("Twitter", "twitter.png", "https://twitter.com/PatEcoFilmFest"),
    Social("Instagram", "instagram.png",
        "https://www.instagram.com/patagoniaecofilmfest/"),
    Social("Youtube", "youtube.png",
        "https://www.youtube.com/channel/UCTEyTVumjWYizxQkvmwh3gg"),
  ];

  _changePage(context, ItemMenu item) {
    onItemTap();
    if (item.id != current) {
      Widget to;

      switch (item.display) {
        case Display.backdrop:
          to = HomePage(
            current: item.id,
            page: item.page,
          );
          break;
        case Display.fullScreen:
          to = item.page;
          break;
      }

      Navigator.pushAndRemoveUntil(
          context,
          MaterialPageRoute(
            builder: (context) => to,
          ), (route) {
        if (item.id == 1) return false;
        return route.isFirst;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Container(
          padding: EdgeInsets.symmetric(vertical: 10.0),
          margin: EdgeInsets.only(top: 10.0),
          // color: Colors.lightGreen[300],
          width: MediaQuery.of(context).size.width,
          child: Wrap(
            runSpacing: 5.0,
            direction: Axis.horizontal,
            alignment: WrapAlignment.center,
            spacing: 5.0,
            children: menu
                .map((item) => RaisedButton.icon(
                      color: current == item.id
                          ? Color(0xFFEA5B23)
                          : Colors.transparent,
                      elevation: current == item.id ? 2.0 : 0.0,
                      icon: Icon(item.icon),
                      label: Text(item.label),
                      textColor: current == item.id
                          ? Colors.white
                          : Colors.black,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(6.0)),
                      onPressed: () {
                        _changePage(context, item);
                      },
                    ))
                .toList(),
          ),
        ),
        // Divider(
        //   height: 20.0,
        // ),
        Expanded(
          child: Container(
            height: 40.0,
            alignment: Alignment.center,
            // child: Text("No hay actividades para hoy"),
          ),
        ),
        Container(
          color: Colors.white,
          padding: EdgeInsets.only(bottom: 50.0, top: 20.0),
          child: Column(
            children: <Widget>[
              Container(
                height: 30.0,
                child: Text(
                  "¡Contanos que te parece el festival!",
                  style: TextStyle(fontSize: 16.0),
                ),
              ),
              Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: social.map((item) => SocialLink(item)).toList()),
            ],
          ),
        ),
      ],
    );
  }
}

class SocialLink extends StatelessWidget {
  final Social social;
  final double height;

  SocialLink(this.social, [this.height = 37.0]);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      child: Image.asset(social.imgPath, height: height),
      onTap: () async {
        if (await canLaunch(social.url))
          await launch(social.url);
        else
          throw 'No se puede acceder a $social.url';
      },
    );
  }
}

enum Display { backdrop, fullScreen }

class ItemMenu {
  int id;
  String label;
  IconData icon;
  Widget page;
  Display display;

  ItemMenu(this.id, this.label, this.icon, this.page, this.display);
}

class Social {
  String red;
  String img;
  String url;

  Social(this.red, this.img, this.url);

  String get imgPath {
    return "assets/$img";
  }
}
