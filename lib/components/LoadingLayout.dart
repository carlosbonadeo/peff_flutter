import 'package:flutter/material.dart';

class LoadingLayout extends StatelessWidget {
  final String label;

  LoadingLayout(this.label);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Card(
        child: Padding(
          padding: const EdgeInsets.fromLTRB(16.0, 22.0, 16.0, 16.0),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              CircularProgressIndicator(),
              label != null
                  ? Padding(
                      padding: EdgeInsets.only(top: 20.0),
                      child: Text(
                        label,
                        style: TextStyle(color: Colors.black87),
                      ),
                    )
                  : Container()
            ],
          ),
        ),
      ),
    );
  }
}
