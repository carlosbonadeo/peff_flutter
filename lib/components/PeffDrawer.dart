import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

class PeffDrawer extends StatelessWidget {
  final String current;

  PeffDrawer([this.current = '']);

  final List<Map<String, Object>> menuArr = [
    {
      "title": "Cronograma",
      "icon": Icons.event,
      "to": "/",
      "current": "Text(\"Cronograma\")"
    },
    {
      "title": "Sede",
      "icon": Icons.location_on,
      "to": "/location",
      "current": "Text(\"Sedes\")"
    },
    {
      "title": "Cartelera",
      "icon": Icons.movie,
      "to": "/movies",
      "current": "Text(\"Cartelera\")"
    },
    {
      "title": "Mis pelis",
      "icon": Icons.favorite,
      "to": "/mispelis",
      "current": "Text(\"Mis pelis\")"
    },
    {
      "title": "Quiénes somos",
      "icon": Icons.face,
      "to": "/staff",
      "current": "/staff"
    },
    {
      "title": "Quiénes nos apoyan",
      "icon": Icons.thumb_up,
      "to": "/apoyo",
      "current": "/apoyo"
    },
    {
      "title": "FAQ",
      "icon": Icons.help_outline,
      "to": "/faq",
      "current": "/faq"
    },
  ];

  final List<Map<String, Object>> social = [
    {
      "img": "face.png",
      "url": "https://www.facebook.com/patagoniaecofilmfest/"
    },
    {"img": "twitter.png", "url": "https://twitter.com/PatEcoFilmFest"},
    {
      "img": "instagram.png",
      "url": "https://www.instagram.com/patagoniaecofilmfest/"
    },
    {
      "img": "youtube.png",
      "url": "https://www.youtube.com/channel/UCTEyTVumjWYizxQkvmwh3gg"
    },
  ];

  @override
  Widget build(BuildContext context) {
    return Drawer(
        child: ListView(padding: EdgeInsets.zero, children: <Widget>[
      UserAccountsDrawerHeader(
        currentAccountPicture: Image.asset("assets/logo_flat.png"),
        accountName: Text(
          "Patagonia Eco Film Fest",
          textScaleFactor: 1.4,
        ),
        accountEmail: Text("Edición 2018"),
      ),
      Container(height: 10.0),
      Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: social
              .map((item) => SocialLink(item["img"], item["url"]))
              .toList()),
      Container(
        height: 10.0,
      ),
      Column(
        children: menuArr
            .map((item) => ListTile(
                  title: Text(item["title"]),
                  leading: Icon(item["icon"]),
                  selected: current == item["current"],
                  onTap: () {
                    Navigator.of(context).pop();
                    if (current != item["to"])
                      Navigator.of(context).pushNamedAndRemoveUntil(
                          item["to"], ModalRoute.withName('/'));
                  },
                ))
            .toList(),
      )
    ]));
  }
}

class SocialLink extends StatelessWidget {
  final String img;
  final String url;
  final double height;

  SocialLink(this.img, this.url, [this.height = 37.0]);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      child: Image.asset("assets/$img", height: height),
      onTap: () async {
        if (await canLaunch(url))
          await launch(url);
        else
          throw 'No se puede acceder a $url';
      },
    );
  }
}
