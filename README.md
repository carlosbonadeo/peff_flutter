# Patagonia Eco Film Fest

Official Patagonia Eco Film Fest app 

<a href='https://play.google.com/store/apps/details?id=com.aleph.peff'>
<img alt='Get it on Google Play' width="200" src='https://play.google.com/intl/en_us/badges/static/images/badges/en_badge_web_generic.png'/>
</a>

![PEFF app](https://i.imgur.com/vZGJ7jw.gif)

## What is PEFF?

The festival established itself as the first environmental film festival of the Patagonia. A selection of the best environmental films and shortfilms of international level will be shown in different parts of the city. The audiovisual productions included in the festival, propose inform, raise awareness and help improve the quality of life of our community and our planet.

## Made with

- Flutter
- Dart

## Todos

- [ ] Add internationalization (i18n)

## License

The MIT License (MIT).

## Contact

- [Github](https://github.com/cdmoro)
- [Gitlab](https://gitlab.com/carlosbonadeo)
- [Linkedin](https://www.linkedin.com/in/cdbonadeo/)
- [Twitter](https://twitter.com/CarlosBonadeo)
- [carlosbonadeo@gmail.com](mailto:carlosbonadeo@gmail.com)
